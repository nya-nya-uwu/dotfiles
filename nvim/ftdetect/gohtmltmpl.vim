let s:cpo_save = &cpo
set cpo&vim

au BufRead,BufNewFile *.html
	\  if search("{{") != 0
		\| set filetype=gohtmltmpl
	\| endif

let &cpo = s:cpo_save
unlet s:cpo_save
