# Recommended from ArchWiki, will only start X11 if there is no active display
# and the virtual terminal number is one.

# TODO: git pull workspace/dotfiles

if [[ ! $DISPLAY ]] && [[ $XDG_VTNR -eq 1 ]]; then
	exec startx
fi
